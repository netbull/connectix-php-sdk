# [2.0.0](https://gitlab.com/netbull/connectix-php-sdk/compare/v1.4.1...v2.0.0) (2022-06-30)


### Bug Fixes

* tests to php8 ([468c0e7](https://gitlab.com/netbull/connectix-php-sdk/commit/468c0e73ef936c33430be5ef07ce73178ef99765))
* tests to php8 ([a43e2ff](https://gitlab.com/netbull/connectix-php-sdk/commit/a43e2ffe18fb7fd7e1a839470e5a221097b4f391))
* tests to php8 ([508af01](https://gitlab.com/netbull/connectix-php-sdk/commit/508af0187d0b52e2be54c48f23679510a020559f))
* tests to php8 ([ad9c8aa](https://gitlab.com/netbull/connectix-php-sdk/commit/ad9c8aa1bc605b64564efb718490942b09468d06))


### Features

* bump version to php 8 ([90dad17](https://gitlab.com/netbull/connectix-php-sdk/commit/90dad172f073e9ef5219dd4b3f31dadc8e4073f6))


### BREAKING CHANGES

* php 8

## [1.4.1](https://gitlab.com/netbull/connectix-php-sdk/compare/v1.4.0...v1.4.1) (2022-06-27)


### Bug Fixes

* added throw tags ([0e204af](https://gitlab.com/netbull/connectix-php-sdk/commit/0e204af1fc85d19a1ac634d1ff0a80f66373d47c))

# [1.4.0](https://gitlab.com/netbull/connectix-php-sdk/compare/v1.3.8...v1.4.0) (2022-06-27)


### Features

* added BadTokenException ([ea121e2](https://gitlab.com/netbull/connectix-php-sdk/commit/ea121e24cd2bd99e73be57101d4399a8422d7fbb))

## [1.3.8](https://gitlab.com/netbull/connectix-php-sdk/compare/v1.3.7...v1.3.8) (2022-06-21)


### Bug Fixes

* going back in time... ([2006fdd](https://gitlab.com/netbull/connectix-php-sdk/commit/2006fdd0026bacd945b36ce6d21550ff7d867d91))

## [1.3.7](https://gitlab.com/netbull/connectix-php-sdk/compare/v1.3.6...v1.3.7) (2021-03-26)


### Bug Fixes

* token and environment change ([443502f](https://gitlab.com/netbull/connectix-php-sdk/commit/443502f92f62f055bba81e435cb080064a033f3a))

## [1.3.6](https://gitlab.com/netbull/connectix-php-sdk/compare/v1.3.5...v1.3.6) (2021-03-25)


### Bug Fixes

* bug with hook validation ([a1f9c11](https://gitlab.com/netbull/connectix-php-sdk/commit/a1f9c11f731f14c63843f17f25c220229a36858d))

## [1.3.5](https://gitlab.com/netbull/connectix-php-sdk/compare/v1.3.4...v1.3.5) (2021-03-25)


### Bug Fixes

* implemented unit tests ([4099980](https://gitlab.com/netbull/connectix-php-sdk/commit/4099980fa9c00a0d4405a91e0e18030f6936a048))

## [1.3.4](https://gitlab.com/netbull/connectix-php-sdk/compare/v1.3.3...v1.3.4) (2021-03-24)


### Bug Fixes

* small refactoring ([23c4820](https://gitlab.com/netbull/connectix-php-sdk/commit/23c48202ee8bad278230b8250b986e0a264bdcfe))

## [1.3.3](https://gitlab.com/netbull/connectix-php-sdk/compare/v1.3.2...v1.3.3) (2021-03-24)


### Bug Fixes

* annotations ([01039bd](https://gitlab.com/netbull/connectix-php-sdk/commit/01039bda2c438b7595ea86a8e62f360e07565843))

## [1.3.2](https://gitlab.com/netbull/connectix-php-sdk/compare/v1.3.1...v1.3.2) (2021-03-24)


### Bug Fixes

* added getters for the request & improved docs ([df7b9aa](https://gitlab.com/netbull/connectix-php-sdk/commit/df7b9aac838ddff3245b95dacd9bfc3bce8a1e48))

## [1.3.1](https://gitlab.com/netbull/connectix-php-sdk/compare/v1.3.0...v1.3.1) (2021-03-23)


### Bug Fixes

* added ToDo item ([3bd509a](https://gitlab.com/netbull/connectix-php-sdk/commit/3bd509a093fc726f6fae5ce6b44bf05f95806976))

# [1.3.0](https://gitlab.com/netbull/connectix-php-sdk/compare/v1.2.1...v1.3.0) (2021-03-23)


### Features

* implemented integration requests ([72ea71c](https://gitlab.com/netbull/connectix-php-sdk/commit/72ea71c82a045438f7695480ce7f89df508e61cc))

## [1.2.1](https://gitlab.com/netbull/connectix-php-sdk/compare/v1.2.0...v1.2.1) (2020-12-25)


### Bug Fixes

* implemented optional query parameters for the requests ([de177b8](https://gitlab.com/netbull/connectix-php-sdk/commit/de177b8e7508a5eb8644678d037d864a23f2c091))

# [1.2.0](https://gitlab.com/netbull/connectix-php-sdk/compare/v1.1.3...v1.2.0) (2020-12-06)


### Features

* added sandbox endpoint ([30a2c08](https://gitlab.com/netbull/connectix-php-sdk/commit/30a2c08764c5d2b48ff24c274b55f75373a542e5))

## [1.1.3](https://gitlab.com/netbull/connectix-php-sdk/compare/v1.1.2...v1.1.3) (2020-11-18)


### Bug Fixes

* CI/CD ([2b60e22](https://gitlab.com/netbull/connectix-php-sdk/commit/2b60e222c7bf3ea2b52d38c9c74f3bf0825912b4))
* CI/CD ([ca2f182](https://gitlab.com/netbull/connectix-php-sdk/commit/ca2f182ab2c0883dc5bdd7c697feeb13bc432bff))
* CI/CD ([e79cd37](https://gitlab.com/netbull/connectix-php-sdk/commit/e79cd37b96aee2298b569b7902b1677c3244b0f7))
