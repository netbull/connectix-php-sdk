# Connectix PHP SDK

## How to use
install the SDK via composer
```shell
$ composer require connectix/php-sdk 
```

Make sure you autoload the classes
```php
require_once __DIR__.'/vendor/autoload.php';
```

Create client
```php
use Connectix\Client;

...

$client = new Client('YOUR_TOKEN_OBTAINED_FROM_CONNECTIX'); // to use in sandbox mode enter second argument "true"
```

To check if a phone number is blacklisted
```php
use Connectix\Components\Blacklist;
use Connectix\Exception;

...

$blacklist = new Blacklist();
$blacklist->setPhone('YOUR_PHONE_NUMBER');

try {
	$isBlacklisted = $client->getBlacklist()->isBlacklisted($blacklist); // returns "yes" or "no"
} catch (Exception\BadConnectionException $e) {
	// Your code
} catch (Exception\BadRequestException $e) {
	// Your code
} catch (Exception\BadResponseBodyException $e) {
	// Your code
}
```

To send a single message
```php
use Connectix\Components\Contact;
use Connectix\Components\Message;
use Connectix\Exception;

...

$contact = new Contact();
$contact->setFirstName('SOME NAME')
	->setLastName('SOME LASTNAME');

$message = new Message();
$message
	->setTemplate('TEMPLATE_ID_FROM_CONNECTIX')  // Required
	->setPhone('YOUR_PHONE_NUMBER')  // Required
	->setParameters([
		'key1' => 'value1',
		'key2' => 'value2'
	])
	->setCallbackUrl('https://YOUR_URL.com')
	->setInboundUrl('https://YOUR_URL.com')
	->setContact($contact);

try {
	$client->getMessages()->send($message);
} catch (Exception\BadConnectionException $e) {
	// Your code
} catch (Exception\BadRequestException $e) {
	// Your code
} catch (Exception\BadResponseBodyException $e) {
	// Your code
}
```

To send a fallback message
```php
use Connectix\Components\Contact;
use Connectix\Components\Fallback;
use Connectix\Components\Message;
use Connectix\Exception;

...

$contact = new Contact();
$contact->setFirstName('SOME NAME')
	->setLastName('SOME LASTNAME');

$fallback = new Fallback();
$fallback
	->setPhone('YOUR_PHONE_NUMBER')  // Required
	->setContact($contact);

$messageOne = new Message();
$messageOne
	->setTemplate('TEMPLATE_ID_FROM_CONNECTIX')  // Required
	->setParameters([
		'key1' => 'value1',
		'key2' => 'value2'
	])
	->setCallbackUrl('https://YOUR_URL.com')
	->setInboundUrl('https://YOUR_URL.com');
$fallback->addMessage($messageOne);

$messageTwo = new Message();
$messageTwo
	->setTemplate('TEMPLATE_ID_FROM_CONNECTIX')  // Required
	->setParameters([
		'key1' => 'value1',
		'key2' => 'value2'
	])
	->setCallbackUrl('https://YOUR_URL.com')
	->setInboundUrl('https://YOUR_URL.com');
$fallback->addMessage($messageTwo);

try {
	$client->getFallback()->send($fallback);
} catch (Exception\BadConnectionException $e) {
	// Your code
} catch (Exception\BadRequestException $e) {
	// Your code
} catch (Exception\BadResponseBodyException $e) {
	// Your code
}
```

To register/unregister integration
```php
use Connectix\Components\Integration;
use Connectix\Components\IntegrationUnregister;
use Connectix\Exception;

...

$integration = new Integration();
$integration
	->setPlatform('YOUR PLATFORM NAME')  // Required
	->setSecret('YOUR 32 CHARACTERS SECRET') // Required
	->setType('custom'); // Required. Available options: ecommerce, automation, custom

try {
	$data = $client->getintegration()->send($integration);
} catch (Exception\BadConnectionException $e) {
	// Your code
} catch (Exception\BadRequestException $e) {
	// Your code
} catch (Exception\BadResponseBodyException $e) {
	// Your code
}

$integrationUnregister = new IntegrationUnregister();
$integrationUnregister
	->setSecret('YOUR 32 CHARACTERS SECRET') // Required. Secret used in the previous request
	->setToken($data['token']); // Required. Token returned from the previous request

try {
	$client->getintegration()->send($integrationUnregister);
} catch (Exception\BadConnectionException $e) {
	// Your code
} catch (Exception\BadRequestException $e) {
	// Your code
} catch (Exception\BadResponseBodyException $e) {
	// Your code
}
```

To register/unregister integration hook
```php
use Connectix\Components\IntegrationHook;
use Connectix\Components\IntegrationHookUnregister;
use Connectix\Exception;

...

$integrationHook = new IntegrationHook();
$integrationHook
	->setToken($data['token'])  // Required. Token obtained from integration registration
	->setUrl('YOUR URL') // Required. Valid URL which will receive calls
	->setType('custom'); // Required. Available options: callback, inbound, template, tracking

try {
	$data = $client->getintegrationHook()->send($integrationHook);
} catch (Exception\BadConnectionException $e) {
	// Your code
} catch (Exception\BadRequestException $e) {
	// Your code
} catch (Exception\BadResponseBodyException $e) {
	// Your code
}

$integrationHookUnregister = new IntegrationHookUnregister();
$integrationHookUnregister
	->setId($data['id']); // Required. Returned from previous request

try {
	$client->getintegrationHook()->send($integrationHookUnregister);
} catch (Exception\BadConnectionException $e) {
	// Your code
} catch (Exception\BadRequestException $e) {
	// Your code
} catch (Exception\BadResponseBodyException $e) {
	// Your code
}
```

# ToDo:
- [x] add request for templates
- [x] add request for messages
    - [x] post messages
    - [ ] get messages
- [ ] add request error handling
- [x] add docs for integrations & hooks
