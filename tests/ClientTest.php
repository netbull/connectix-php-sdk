<?php

namespace Connectix\Tests;

use Connectix\Client;
use Connectix\Exception\NoTokenException;
use PHPUnit\Framework\TestCase;

final class ClientTest extends TestCase
{
	const TOKENS = ['a', 'b'];

	public function testInitializingWithoutProvidedToken()
	{
		$this->expectException(NoTokenException::class);
		new Client('');
	}

	public function testRequestGetter()
	{
		$client = new Client('test-token');

		$requests = ['blacklist', 'templates', 'messages', 'fallback', 'countries', 'integration', 'integrationHook'];
		foreach ($requests as $requestName) {
			$requestName = ucfirst($requestName);
			$request = $client->{'get'.$requestName}();
			$expected = "Connectix\\Request\\{$requestName}Request";
			$this->assertInstanceOf($expected, $request, "Testing request getter: $requestName");
		}
	}

	public function testTokenChange()
	{
		$client = new Client('test-token');

		$client->setToken('new-token');
		$requests = ['blacklist', 'templates', 'messages', 'fallback', 'countries', 'integration', 'integrationHook'];
		foreach ($requests as $requestName) {
			$requestName = ucfirst($requestName);
			$request = $client->{'get'.$requestName}();
			$this->assertEquals('new-token', $request->getToken());
		}
	}

	public function testSystemChange()
	{
		$client = new Client('test-token');

		$client->setIsDev(false);
		$requests = ['blacklist', 'templates', 'messages', 'fallback', 'countries', 'integration', 'integrationHook'];
		foreach ($requests as $requestName) {
			$requestName = ucfirst($requestName);
			$request = $client->{'get'.$requestName}();
			$this->assertEquals(false, $request->isDev());
		}
	}
}
