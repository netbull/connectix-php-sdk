<?php

namespace Connectix\Tests\Components;

use Connectix\Components;
use Connectix\Exception\RequiredValueException;
use Connectix\Exception\ValueException;
use PHPUnit\Framework\TestCase;

class FallbackTest extends TestCase
{
	public function testRequiredFields()
	{
		$this->expectException(RequiredValueException::class);

		$data = new Components\Fallback();
		$data->toArray();
	}

	public function testConvertingToArray()
	{
		$data = new Components\Fallback();
		$data->setPhone(MessageTest::PHONE);

		$contact = new Components\Contact();
		$contact->setFirstName(ContactTest::FIRST_NAME)
			->setLastName(ContactTest::LAST_NAME);

		$data->setContact($contact);

		$messageOne = new Components\Message();
		$messageOne->setTemplate(MessageTest::UUIDS[0]);
		$data->addMessage($messageOne);

		$messageTwo = new Components\Message();
		$messageTwo->setTemplate(MessageTest::UUIDS[1]);
		$data->addMessage($messageTwo);

		$expected = [
			'phone' => MessageTest::PHONE,
			'flow' => [
				[
					'template' => MessageTest::UUIDS[0],
					'position' => 0,
				],
				[
					'template' => MessageTest::UUIDS[1],
					'position' => 1,
				]
			],
			'contact' => [
				'firstName' => ContactTest::FIRST_NAME,
				'lastName' => ContactTest::LAST_NAME,
				'addGroupIfMissing' => true,
			],
		];
		$result = $data->toArray();
		$this->assertJsonStringEqualsJsonString(json_encode($expected), json_encode($result));
	}

	public function testConvertingToJson()
	{
		$data = new Components\Fallback();
		$data->setPhone(MessageTest::PHONE);

		$contact = new Components\Contact();
		$contact->setFirstName(ContactTest::FIRST_NAME)
			->setLastName(ContactTest::LAST_NAME);

		$data->setContact($contact);

		$messageOne = new Components\Message();
		$messageOne->setTemplate(MessageTest::UUIDS[0]);
		$data->addMessage($messageOne);

		$messageTwo = new Components\Message();
		$messageTwo->setTemplate(MessageTest::UUIDS[1]);
		$data->addMessage($messageTwo);

		$expected = [
			'phone' => MessageTest::PHONE,
			'flow' => [
				[
					'template' => MessageTest::UUIDS[0],
					'position' => 0,
				],
				[
					'template' => MessageTest::UUIDS[1],
					'position' => 1,
				]
			],
			'contact' => [
				'firstName' => ContactTest::FIRST_NAME,
				'lastName' => ContactTest::LAST_NAME,
				'addGroupIfMissing' => true,
			],
		];
		$this->assertEquals($data->toJson(), json_encode($expected));
	}

	public function testFlowConstrain()
	{
		$data = new Components\Fallback();
		$data->setPhone(MessageTest::PHONE);

		$message = new Components\Message();
		$message->setTemplate(MessageTest::UUIDS[0]);
		$data->addMessage($message);

		$this->expectException(ValueException::class);
		$data->toArray();
	}

	public function testDuplicatedTemplatesUsage()
	{
		$data = new Components\Fallback();
		$data->setPhone(MessageTest::PHONE);

		$messageOne = new Components\Message();
		$messageOne->setTemplate(MessageTest::UUIDS[0]);
		$data->addMessage($messageOne);

		$messageTwo = new Components\Message();
		$messageTwo->setTemplate(MessageTest::UUIDS[0]);
		$data->addMessage($messageTwo);

		$this->expectException(ValueException::class);
		$data->toArray();
	}
}
