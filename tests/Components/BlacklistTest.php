<?php

namespace Connectix\Tests\Components;

use Connectix\Components\Blacklist;
use Connectix\Exception\RequiredValueException;
use PHPUnit\Framework\TestCase;

class BlacklistTest extends TestCase
{
	public function testRequiredFields()
	{
		$this->expectException(RequiredValueException::class);

		$data = new Blacklist();
		$data->toArray();
	}

	public function testConvertingToArray()
	{
		$data = new Blacklist();
		$data->setPhone(MessageTest::PHONE);

		$expected = ['phone' => MessageTest::PHONE];
		$result = $data->toArray();
		$this->assertJsonStringEqualsJsonString(json_encode($expected), json_encode($result));
	}

	public function testConvertingToJson()
	{
		$data = new Blacklist();
		$data->setPhone(MessageTest::PHONE);

		$this->assertEquals($data->toJson(), json_encode(['phone' => MessageTest::PHONE]));
	}
}
