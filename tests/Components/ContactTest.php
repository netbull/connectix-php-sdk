<?php

namespace Connectix\Tests\Components;

use Connectix\Components\Contact;
use Connectix\Exception\RequiredValueException;
use PHPUnit\Framework\TestCase;

class ContactTest extends TestCase
{
	const FIRST_NAME = 'Testin';
	const LAST_NAME = 'Testov';

	public function testRequiredFields()
	{
		$this->expectException(RequiredValueException::class);

		$data = new Contact();
		$data->toArray();
	}

	public function testConvertingToArray()
	{
		$data = new Contact();
		$data->setFirstName(self::FIRST_NAME)
			->setLastName(self::LAST_NAME);

		$expected = [
			'firstName' => self::FIRST_NAME,
			'lastName' => self::LAST_NAME,
			'addGroupIfMissing' => true,
		];
		$result = $data->toArray();
		$this->assertJsonStringEqualsJsonString(json_encode($expected), json_encode($result));
	}

	public function testConvertingToJson()
	{
		$data = new Contact();
		$data->setFirstName(self::FIRST_NAME)
			->setLastName(self::LAST_NAME);

		$expected = [
			'firstName' => self::FIRST_NAME,
			'lastName' => self::LAST_NAME,
			'addGroupIfMissing' => true,
		];
		$this->assertEquals($data->toJson(), json_encode($expected));
	}

	public function testNameCompositionJson()
	{
		$data = new Contact();
		$data->setFirstName(self::FIRST_NAME);

		$this->assertEquals($data->getName(), self::FIRST_NAME);

		$data->setLastName(self::LAST_NAME);
		$this->assertEquals($data->getName(), self::FIRST_NAME.' '.self::LAST_NAME);
	}
}
