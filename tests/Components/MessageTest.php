<?php

namespace Connectix\Tests\Components;

use Connectix\Components;
use Connectix\Exception\RequiredValueException;
use Connectix\Exception\ValueException;
use PHPUnit\Framework\TestCase;

class MessageTest extends TestCase
{
	const PHONE = '+359123456789';
	const URL = 'https://domain.ltd';
	const UUIDS = [
		'47cd85aa-b41c-48e4-a2dc-7b5e5d0613b7',
		'b60edca4-84f0-43d8-8a47-249be4272402',
	];

	public function testRequiredFields()
	{
		$this->expectException(RequiredValueException::class);

		$data = new Components\Message();
		$data->toArray();
	}

	public function testConvertingToArray()
	{
		$data = new Components\Message();
		$data->setPhone(self::PHONE)
			->setTemplate(self::UUIDS[0])
			->setCallbackUrl(self::URL)
			->setInboundUrl(self::URL);

		$contact = new Components\Contact();
		$contact->setFirstName(ContactTest::FIRST_NAME)
			->setLastName(ContactTest::LAST_NAME);

		$data->setContact($contact);

		$expected = [
			'template' => self::UUIDS[0],
			'callbackUrl' => self::URL,
			'inboundUrl' => self::URL,
			'phone' => self::PHONE,
			'contact' => [
				'firstName' => ContactTest::FIRST_NAME,
				'lastName' => ContactTest::LAST_NAME,
				'addGroupIfMissing' => true,
			],
		];
		$result = $data->toArray();

		$this->assertJsonStringEqualsJsonString(json_encode($expected), json_encode($result));
	}

	public function testConvertingToJson()
	{
		$data = new Components\Message();
		$data->setPhone(self::PHONE)
			->setTemplate(self::UUIDS[0])
			->setCallbackUrl(self::URL)
			->setInboundUrl(self::URL);

		$contact = new Components\Contact();
		$contact->setFirstName(ContactTest::FIRST_NAME)
			->setLastName(ContactTest::LAST_NAME);

		$data->setContact($contact);

		$expected = [
			'template' => self::UUIDS[0],
			'callbackUrl' => self::URL,
			'inboundUrl' => self::URL,
			'phone' => self::PHONE,
			'contact' => [
				'firstName' => ContactTest::FIRST_NAME,
				'lastName' => ContactTest::LAST_NAME,
				'addGroupIfMissing' => true,
			],
		];
		$this->assertEquals($data->toJson(), json_encode($expected));
	}

	public function testTemplateIdConstrain()
	{
		$data = new Components\Message();
		$data->setPhone(self::PHONE)
			->setTemplate('something-as-fake-id');

		$this->expectException(ValueException::class);
		$data->toArray();
	}

	public function testHookUrlsConstrain()
	{
		$data = new Components\Message();
		$data->setPhone(self::PHONE)
			->setTemplate(self::UUIDS[0]);

		$data->setCallbackUrl('domain.ltd');

		$this->expectException(ValueException::class);
		$data->toArray();

		$data->setCallbackUrl(null);
		$data->setInboundUrl('domain.ltd');
		$data->toArray();
	}
}
