<?php

namespace Connectix;

use Connectix\Exception\NoTokenException;
use Connectix\Request;

class Client
{
	/**
	 * @var string
	 */
	private string $token = '';

	/**
	 * @var bool
	 */
	private bool $isDev = true;

	/**
	 * @var Request\RequestInterface[]
	 */
	private array $requests = [];

	/**
	 * @param string|null $token
	 * @param bool $isDev
	 * @throws NoTokenException
	 */
	public function __construct(?string $token, bool $isDev = true)
	{
		if (empty($token)) {
			throw new NoTokenException;
		}

		$this->token = $token;
		$this->isDev = $isDev;
	}

	/**
	 * @return string
	 */
	public function getToken(): string
	{
		return $this->token;
	}

	/**
	 * @param string $token
	 * @return $this
	 */
	public function setToken(string $token): self
	{
		$this->token = $token;
		foreach ($this->requests as $request) {
			$request->setToken($token);
		}

		return $this;
	}

	/**
	 * @return bool
	 */
	public function isDev(): bool
	{
		return $this->isDev;
	}

	/**
	 * @param bool $isDev
	 * @return Client
	 */
	public function setIsDev(bool $isDev): self
	{
		$this->isDev = $isDev;
		foreach ($this->requests as $request) {
			$request->setIsDev($isDev);
		}

		return $this;
	}

	/**
	 * @param string $name
	 * @return Request\RequestInterface|mixed
	 */
	private function getRequest(string $name)
	{
		if (!isset($this->requests[$name])) {
			$class = 'Connectix\\Request\\'.ucfirst($name).'Request';
			$this->requests[$name] = new $class($this->token, $this->isDev);
		}

		return $this->requests[$name];
	}

	/**
	 * @return Request\BlacklistRequest
	 */
	public function getBlacklist(): Request\BlacklistRequest
	{
		return $this->getRequest('blacklist');
	}

	/**
	 * @return Request\TemplatesRequest
	 */
	public function getTemplates(): Request\TemplatesRequest
	{
		return $this->getRequest('templates');
	}

	/**
	 * @return Request\MessagesRequest
	 */
	public function getMessages(): Request\RequestInterface
	{
		return $this->getRequest('messages');
	}

	/**
	 * @return Request\FallbackRequest
	 */
	public function getFallback(): Request\RequestInterface
	{
		return $this->getRequest('fallback');
	}

	/**
	 * @return Request\CountriesRequest
	 */
	public function getCountries(): Request\CountriesRequest
	{
		return $this->getRequest('countries');
	}

	/**
	 * @return Request\IntegrationRequest
	 */
	public function getIntegration(): Request\RequestInterface
	{
		return $this->getRequest('integration');
	}

	/**
	 * @return Request\IntegrationHookRequest
	 */
	public function getIntegrationHook(): Request\IntegrationHookRequest
	{
		return $this->getRequest('integrationHook');
	}
}
