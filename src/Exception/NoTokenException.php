<?php

namespace Connectix\Exception;

use Exception;

class NoTokenException extends Exception
{
	public function __construct()
	{
		parent::__construct('The token should be provided.');
	}
}
