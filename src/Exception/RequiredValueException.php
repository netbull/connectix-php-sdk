<?php

namespace Connectix\Exception;

use Exception;

class RequiredValueException extends Exception
{
	/**
	 * @param string $field
	 */
	public function __construct(string $field)
	{
		parent::__construct("Field \"{$field}\" should have a value.");
	}
}
