<?php

namespace Connectix\Exception;

use Exception;

class BadRequestException extends Exception
{
	/**
	 * @param int $statusCode
	 * @param string $message
	 */
	public function __construct(int $statusCode, string $message)
	{
		parent::__construct("{$statusCode}: {$message}.");
	}
}
