<?php

namespace Connectix\Exception;

use Exception;

class BadConnectionException extends Exception
{
	public function __construct()
	{
		parent::__construct('Something broke over the network and the request to the API was not made.');
	}
}
