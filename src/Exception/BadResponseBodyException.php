<?php

namespace Connectix\Exception;

use Exception;

class BadResponseBodyException extends Exception
{
	public function __construct()
	{
		parent::__construct('The API returned malformed response body.');
	}
}
