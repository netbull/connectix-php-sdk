<?php

namespace Connectix\Exception;

use Exception;

class BadTokenException extends Exception
{
	public function __construct()
	{
		parent::__construct('The provided token is not valid');
	}
}
