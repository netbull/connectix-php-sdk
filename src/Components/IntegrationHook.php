<?php

namespace Connectix\Components;

use Connectix\Exception\RequiredValueException;
use Connectix\Exception\ValueException;

final class IntegrationHook extends BaseComponent
{
	const TYPE_CALLBACK = 'callback';
	const TYPE_INBOUND = 'inbound';
	const TYPE_TEMPLATE = 'template';
	const TYPE_TRACKING = 'tracking';

	/**
	 * @var string
	 */
	protected string $type = self::TYPE_CALLBACK;

	/**
	 * @var string|null
	 */
	protected ?string $token = null;

	/**
	 * @var string|null
	 */
	protected ?string $url = null;

	/**
	 * @inheritDoc
	 */
	public function getRequiredFields(): array
	{
		return ['type', 'token', 'url'];
	}

	/**
	 * @throws RequiredValueException
	 * @throws ValueException
	 */
	protected function validate()
	{
		parent::validate();

		if (false === filter_var($this->getUrl(), FILTER_VALIDATE_URL)) {
			throw new ValueException("Value \"".$this->getUrl()."\" is not a valid URL.");
		}

		$allowedValues = [self::TYPE_CALLBACK, self::TYPE_INBOUND, self::TYPE_TEMPLATE, self::TYPE_TRACKING];
		if (!in_array($this->getType(), $allowedValues)) {
			throw new ValueException("Value \"".$this->getType()."\" is not valid. Allowed values are: ".join(', ', $allowedValues));
		}
	}

	/**
	 * @return array
	 * @throws RequiredValueException
	 * @throws ValueException
	 */
	public function toArray(): array
	{
		$this->validate();

		return [
			'type' => $this->getType(),
			'token' => $this->getToken(),
			'url' => $this->getUrl(),
		];
	}

	/**
	 * @return string
	 * @throws RequiredValueException
	 * @throws ValueException
	 */
	public function toJson(): string
	{
		if ($data = json_encode($this->toArray())) {
			return $data;
		}

		return '';
	}

	/**
	 * @return string
	 */
	public function getType(): string
	{
		return $this->type;
	}

	/**
	 * @param string $type
	 * @return $this
	 */
	public function setType(string $type): self
	{
		$this->type = $type;

		return $this;
	}

	/**
	 * @return string|null
	 */
	public function getToken(): ?string
	{
		return $this->token;
	}

	/**
	 * @param string $token
	 * @return IntegrationHook
	 */
	public function setToken(string $token): self
	{
		$this->token = $token;

		return $this;
	}

	/**
	 * @return string|null
	 */
	public function getUrl(): ?string
	{
		return $this->url;
	}

	/**
	 * @param string $url
	 * @return $this
	 */
	public function setUrl(string $url): self
	{
		$this->url = $url;

		return $this;
	}
}
