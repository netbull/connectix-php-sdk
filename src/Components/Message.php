<?php

namespace Connectix\Components;

use Connectix\Exception\RequiredValueException;
use Connectix\Exception\ValueException;

final class Message extends BaseComponent
{
	/**
	 * @var int|null
	 */
	protected ?int $ttl = null;

	/**
	 * @var string|null
	 */
	protected ?string $callbackUrl = null;

	/**
	 * @var string|null
	 */
	protected ?string $inboundUrl = null;

	/**
	 * @var string|null
	 */
	protected ?string $template = null;

	/**
	 * @var array
	 */
	protected array $parameters = [];

	/**
	 * @var Contact|null
	 */
	protected ?Contact $contact = null;

	/**
	 * @var string|null
	 */
	protected ?string $phone = null;

	/**
	 * @var int|null
	 */
	protected ?int $position = null;

	/**
	 * @var bool
	 */
	protected bool $compound = false;

	/**
	 * @inheritDoc
	 */
	public function getRequiredFields(): array
	{
		$fields = ['template'];
		if ($this->isCompound()) {
			$fields[] = 'position';
		} else {
			$fields[] = 'phone';
		}

		return $fields;
	}

	/**
	 * @throws RequiredValueException
	 * @throws ValueException
	 */
	protected function validate()
	{
		parent::validate();

		if (!is_string($this->getTemplate()) || !preg_match('/^[a-f\d]{8}(-[a-f\d]{4}){4}[a-f\d]{8}$/i', $this->getTemplate())) {
			throw new ValueException("Value \"".$this->getTemplate()."\" is not a valid template ID.");
		}

		if ($this->getCallbackUrl() && false === filter_var($this->getCallbackUrl(), FILTER_VALIDATE_URL)) {
			throw new ValueException("Value \"".$this->getCallbackUrl()."\" is not a valid URL.");
		}

		if ($this->getInboundUrl() && false === filter_var($this->getInboundUrl(), FILTER_VALIDATE_URL)) {
			throw new ValueException("Value \"".$this->getInboundUrl()."\" is not a valid URL.");
		}
	}

	/**
	 * @return string[]
	 * @throws RequiredValueException
	 * @throws ValueException
	 */
	public function toArray(): array
	{
		$this->validate();

		$params = [
			'template' => $this->getTemplate(),
		];

		if (null !== $this->getTtl()) {
			$params['ttl'] = $this->getTtl();
		}

		if (null !== $this->getCallbackUrl()) {
			$params['callbackUrl'] = $this->getCallbackUrl();
		}

		if (null !== $this->getInboundUrl()) {
			$params['inboundUrl'] = $this->getInboundUrl();
		}

		if (!empty($this->getParameters())) {
			$params['parameters'] = $this->getParameters();
		}

		if ($this->isCompound()) {
			$params['position'] = $this->getPosition();
		} else {
			$params['phone'] = $this->getPhone();

			if (null !== $this->getContact()) {
				$params['contact'] = $this->getContact()->toArray();
			}
		}

		return $params;
	}

	/**
	 * @inheritDoc
	 * @throws ValueException
	 */
	public function toJson(): string
	{
		if ($data = json_encode($this->toArray())) {
			return $data;
		}

		return '';
	}

	/**
	 * @return int|null
	 */
	public function getTtl(): ?int
	{
		return $this->ttl;
	}

	/**
	 * @param int $ttl
	 * @return Message
	 */
	public function setTtl(int $ttl): self
	{
		$this->ttl = $ttl;

		return $this;
	}

	/**
	 * @return string|null
	 */
	public function getCallbackUrl(): ?string
	{
		return $this->callbackUrl;
	}

	/**
	 * @param string|null $callbackUrl
	 * @return Message
	 */
	public function setCallbackUrl(?string $callbackUrl): self
	{
		$this->callbackUrl = $callbackUrl;

		return $this;
	}

	/**
	 * @return string|null
	 */
	public function getInboundUrl(): ?string
	{
		return $this->inboundUrl;
	}

	/**
	 * @param string|null $inboundUrl
	 * @return Message
	 */
	public function setInboundUrl(?string $inboundUrl): self
	{
		$this->inboundUrl = $inboundUrl;

		return $this;
	}

	/**
	 * @return string|null
	 */
	public function getTemplate(): ?string
	{
		return $this->template;
	}

	/**
	 * @param string $template
	 * @return Message
	 */
	public function setTemplate(string $template): self
	{
		$this->template = $template;

		return $this;
	}

	/**
	 * @return array
	 */
	public function getParameters(): array
	{
		return $this->parameters;
	}

	/**
	 * @param array $parameters
	 * @return Message
	 */
	public function setParameters(array $parameters): self
	{
		// Make sure that all parameters are strings
		$this->parameters = array_map(function ($param) {
			return (string)$param;
		}, $parameters);

		return $this;
	}

	/**
	 * @return Contact|null
	 */
	public function getContact(): ?Contact
	{
		return $this->contact;
	}

	/**
	 * @param Contact|null $contact
	 * @return Message
	 */
	public function setContact(?Contact $contact): self
	{
		$this->contact = $contact;

		return $this;
	}

	/**
	 * @return string|null
	 */
	public function getPhone(): ?string
	{
		return $this->phone;
	}

	/**
	 * @param string|null $phone
	 * @return Message
	 */
	public function setPhone(?string $phone): self
	{
		$this->phone = $phone;

		return $this;
	}

	/**
	 * @return int|null
	 */
	public function getPosition(): ?int
	{
		return $this->position;
	}

	/**
	 * @param int|null $position
	 * @return Message
	 */
	public function setPosition(?int $position): self
	{
		$this->position = $position;

		return $this;
	}

	/**
	 * @return bool
	 */
	public function isCompound(): bool
	{
		return $this->compound;
	}

	/**
	 * @param bool $compound
	 * @return Message
	 */
	public function setCompound(bool $compound): self
	{
		$this->compound = $compound;

		if ($compound) {
			$this->setPhone(null);
			$this->setContact(null);
		}

		return $this;
	}
}
