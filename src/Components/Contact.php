<?php

namespace Connectix\Components;

use Connectix\Exception\RequiredValueException;

final class Contact extends BaseComponent
{
	/**
	 * @var bool
	 */
	protected bool $addGroupIfMissing = true;

	/**
	 * @var string|null
	 */
	protected ?string $firstName = null;

	/**
	 * @var string|null
	 */
	protected ?string $lastName = null;

	/**
	 * @var string[]
	 */
	protected array $groups = [];

	/**
	 * @inheritDoc
	 */
	public function getRequiredFields(): array
	{
		return ['firstName'];
	}

	/**
	 * @return string[]
	 * @throws RequiredValueException
	 */
	public function toArray(): array
	{
		$this->validate();

		$params = [
			'firstName' => $this->getFirstName(),
		];

		if (null !== $this->getLastName()) {
			$params['lastName'] = $this->getLastName();
		}

		if (!empty($this->getGroups())) {
			$params['groups'] = $this->getGroups();
		}

		$params['addGroupIfMissing'] = $this->isAddGroupIfMissing();

		return $params;
	}

	/**
	 * @inheritDoc
	 */
	public function toJson(): string
	{
		if ($data = json_encode($this->toArray())) {
			return $data;
		}

		return '';
	}

	/**
	 * @return bool
	 */
	public function isAddGroupIfMissing(): bool
	{
		return $this->addGroupIfMissing;
	}

	/**
	 * @param bool $addGroupIfMissing
	 * @return Contact
	 */
	public function setAddGroupIfMissing(bool $addGroupIfMissing): self
	{
		$this->addGroupIfMissing = $addGroupIfMissing;

		return $this;
	}

	/**
	 * @return string|null
	 */
	public function getFirstName(): ?string
	{
		return $this->firstName;
	}

	/**
	 * @param string|null $firstName
	 * @return Contact
	 */
	public function setFirstName(?string $firstName): self
	{
		$this->firstName = $firstName;

		return $this;
	}

	/**
	 * @return string|null
	 */
	public function getLastName(): ?string
	{
		return $this->lastName;
	}

	/**
	 * @param string|null $lastName
	 * @return self
	 */
	public function setLastName(?string $lastName): self
	{
		$this->lastName = $lastName;

		return $this;
	}

	/**
	 * @return string[]
	 */
	public function getGroups(): array
	{
		return $this->groups;
	}

	/**
	 * @param string[] $groups
	 * @return Contact
	 */
	public function setGroups(array $groups): self
	{
		$this->groups = $groups;

		return $this;
	}

	/**
	 * @return string
	 */
	public function getName(): string
	{
		$names = [$this->getFirstName()];
		if ($this->getLastName()) {
			$names[] = $this->getLastName();
		}

		return join(' ', $names);
	}
}
