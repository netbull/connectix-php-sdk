<?php

namespace Connectix\Components;

use Connectix\Exception\RequiredValueException;
use Connectix\Exception\ValueException;

final class IntegrationUnregister extends BaseComponent
{
	/**
	 * @var string|null
	 */
	protected ?string $token = null;

	/**
	 * @var string|null
	 */
	protected ?string $secret = null;

	/**
	 * @inheritDoc
	 */
	public function getRequiredFields(): array
	{
		return ['token', 'secret'];
	}

	/**
	 * @throws RequiredValueException
	 * @throws ValueException
	 */
	protected function validate()
	{
		parent::validate();

		if (32 !== strlen($this->getSecret())) {
			throw new ValueException("Value \"".$this->getSecret()."\" should be 32 characters long.");
		}
	}

	/**
	 * @return array
	 * @throws RequiredValueException
	 * @throws ValueException
	 */
	public function toArray(): array
	{
		$this->validate();

		return [
			'token' => $this->getToken(),
			'secret' => $this->getSecret(),
		];
	}

	/**
	 * @return string
	 * @throws RequiredValueException
	 * @throws ValueException
	 */
	public function toJson(): string
	{
		if ($data = json_encode($this->toArray())) {
			return $data;
		}

		return '';
	}

	/**
	 * @return string|null
	 */
	public function getToken(): ?string
	{
		return $this->token;
	}

	/**
	 * @param string $token
	 * @return IntegrationUnregister
	 */
	public function setToken(string $token): self
	{
		$this->token = $token;

		return $this;
	}

	/**
	 * @return string|null
	 */
	public function getSecret(): ?string
	{
		return $this->secret;
	}

	/**
	 * @param string $secret
	 * @return $this
	 */
	public function setSecret(string $secret): self
	{
		$this->secret = $secret;

		return $this;
	}
}
