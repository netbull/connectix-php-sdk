<?php

namespace Connectix\Components;

use Connectix\Exception\RequiredValueException;

abstract class BaseComponent implements ComponentInterface
{
	/**
	 * @inheritDoc
	 */
	public function getRequiredFields(): array
	{
		return [];
	}

	/**
	 * @throws RequiredValueException
	 */
	protected function validate()
	{
		foreach ($this->getRequiredFields() as $field) {
			$value = $this->{'get'.ucfirst($field)}();
			if (null === $value || '' === $value || (is_array($value) && 0 === sizeof($value))) {
				throw new RequiredValueException($field);
			}
		}
	}

	/**
	 * @return string
	 * @throws RequiredValueException
	 */
	public function toJson(): string
	{
		$this->validate();

		return '';
	}
}
