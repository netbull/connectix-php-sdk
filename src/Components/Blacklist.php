<?php

namespace Connectix\Components;

use Connectix\Exception\RequiredValueException;

final class Blacklist extends BaseComponent
{
	/**
	 * @var string|null
	 */
	protected ?string $phone = null;

	/**
	 * @inheritDoc
	 */
	public function getRequiredFields(): array
	{
		return ['phone'];
	}

	/**
	 * @return string[]
	 * @throws RequiredValueException
	 */
	public function toArray(): array
	{
		parent::validate();

		return [
			'phone' => $this->getPhone(),
		];
	}

	/**
	 * @inheritDoc
	 */
	public function toJson(): string
	{
		if ($data = json_encode($this->toArray())) {
			return $data;
		}

		return '';
	}

	/**
	 * @return string|null
	 */
	public function getPhone(): ?string
	{
		return $this->phone;
	}

	/**
	 * @param string $phone
	 * @return Blacklist
	 */
	public function setPhone(string $phone): Blacklist
	{
		$this->phone = $phone;

		return $this;
	}
}
