<?php

namespace Connectix\Components;

use Connectix\Exception\RequiredValueException;
use Connectix\Exception\ValueException;

final class Integration extends BaseComponent
{
	const TYPE_ECOMMERCE = 'ecommerce';
	const TYPE_AUTOMATION = 'automation';
	const TYPE_CUSTOM = 'custom';

	/**
	 * @var string
	 */
	protected string $type = self::TYPE_CUSTOM;

	/**
	 * @var string|null
	 */
	protected ?string $platform = null;

	/**
	 * @var string|null
	 */
	protected ?string $secret = null;

	/**
	 * @inheritDoc
	 */
	public function getRequiredFields(): array
	{
		return ['type', 'platform', 'secret'];
	}

	/**
	 * @throws RequiredValueException
	 * @throws ValueException
	 */
	protected function validate()
	{
		parent::validate();

		if (32 !== strlen($this->getSecret())) {
			throw new ValueException("Value \"".$this->getSecret()."\" should be 32 characters long.");
		}

		$allowedValues = [self::TYPE_ECOMMERCE, self::TYPE_AUTOMATION, self::TYPE_CUSTOM];
		if (!in_array($this->getType(), $allowedValues)) {
			throw new ValueException("Value \"".$this->getType()."\" is not valid. Allowed values are: ".join(', ', $allowedValues));
		}
	}

	/**
	 * @return array
	 * @throws RequiredValueException
	 * @throws ValueException
	 */
	public function toArray(): array
	{
		$this->validate();

		return [
			'type' => $this->getType(),
			'platform' => $this->getPlatform(),
			'secret' => $this->getSecret(),
		];
	}

	/**
	 * @return string
	 * @throws RequiredValueException
	 * @throws ValueException
	 */
	public function toJson(): string
	{
		if ($data = json_encode($this->toArray())) {
			return $data;
		}

		return '';
	}

	/**
	 * @return string
	 */
	public function getType(): string
	{
		return $this->type;
	}

	/**
	 * @param string $type
	 * @return $this
	 */
	public function setType(string $type): self
	{
		$this->type = $type;

		return $this;
	}

	/**
	 * @return string|null
	 */
	public function getPlatform(): ?string
	{
		return $this->platform;
	}

	/**
	 * @param string $platform
	 * @return Integration
	 */
	public function setPlatform(string $platform): self
	{
		$this->platform = $platform;

		return $this;
	}

	/**
	 * @return string|null
	 */
	public function getSecret(): ?string
	{
		return $this->secret;
	}

	/**
	 * @param string $secret
	 * @return Integration
	 */
	public function setSecret(string $secret): self
	{
		$this->secret = $secret;

		return $this;
	}
}
