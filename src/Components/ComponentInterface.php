<?php

namespace Connectix\Components;

interface ComponentInterface
{
	/**
	 * @return array
	 */
	public function getRequiredFields(): array;

	/**w
	 * @return array
	 */
	public function toArray(): array;

	/**
	 * @return string
	 */
	public function toJson(): string;
}
