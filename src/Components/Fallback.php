<?php

namespace Connectix\Components;

use Connectix\Exception\RequiredValueException;
use Connectix\Exception\ValueException;

final class Fallback extends BaseComponent
{
	/**
	 * @var Contact|null
	 */
	protected ?Contact $contact;

	/**
	 * @var string
	 */
	protected string $phone = '';

	/**
	 * @var Message[]
	 */
	protected array $flow = [];

	/**
	 * @inheritDoc
	 */
	public function getRequiredFields(): array
	{
		return ['phone', 'flow'];
	}

	/**
	 * @throws RequiredValueException
	 * @throws ValueException
	 */
	protected function validate()
	{
		parent::validate();

		if (2 > sizeof($this->getFlow())) {
			throw new ValueException("Value \"flow\" should have 2 or more elements");
		}

		$templates = [];
		foreach ($this->getFlow() as $i => $message) {
			if (in_array($message->getTemplate(), $templates)) {
				throw new ValueException("\"flow\" item \"$i\" contains duplicated template ".$message->getTemplate());
			}

			$templates[] = $message->getTemplate();
		}
	}


	/**
	 * @return array
	 * @throws RequiredValueException
	 * @throws ValueException
	 */
	public function toArray(): array
	{
		$this->validate();

		$params = [
			'phone' => $this->getPhone(),
			'flow' => [],
		];

		if (null !== $this->getContact()) {
			$params['contact'] = $this->getContact()->toArray();
		}

		foreach ($this->getFlow() as $message) {
			$params['flow'][] = $message->toArray();
		}
		return $params;
	}

	/**
	 * @return string
	 * @throws RequiredValueException
	 * @throws ValueException
	 */
	public function toJson(): string
	{
		if ($data = json_encode($this->toArray())) {
			return $data;
		}

		return '';
	}

	/**
	 * @return Contact|null
	 */
	public function getContact(): ?Contact
	{
		return $this->contact;
	}

	/**
	 * @param Contact|null $contact
	 * @return Fallback
	 */
	public function setContact(Contact $contact = null): self
	{
		$this->contact = $contact;

		return $this;
	}

	/**
	 * @return string
	 */
	public function getPhone(): string
	{
		return $this->phone;
	}

	/**
	 * @param string $phone
	 * @return Fallback
	 */
	public function setPhone(string $phone): self
	{
		$this->phone = $phone;

		return $this;
	}

	/**
	 * @return Message[]
	 */
	public function getFlow(): array
	{
		return $this->flow;
	}

	/**
	 * @param Message[] $flow
	 * @return Fallback
	 */
	public function setFlow(array $flow): self
	{
		$this->flow = $flow;

		return $this;
	}

	/**
	 * @param Message $message
	 * @return $this
	 */
	public function addMessage(Message $message): self
	{
		$message->setCompound(true)
			->setPosition(count($this->flow));
		$this->flow[] = $message;

		return $this;
	}
}
