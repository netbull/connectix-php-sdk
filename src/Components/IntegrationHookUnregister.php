<?php

namespace Connectix\Components;

use Connectix\Exception\RequiredValueException;
use Connectix\Exception\ValueException;

final class IntegrationHookUnregister extends BaseComponent
{
	/**
	 * @var string|null
	 */
	protected ?string $id = null;

	/**
	 * @inheritDoc
	 */
	public function getRequiredFields(): array
	{
		return ['id'];
	}

	/**
	 * @throws RequiredValueException
	 * @throws ValueException
	 */
	protected function validate()
	{
		parent::validate();

		if (!is_string($this->getId()) || !preg_match('/^[a-f\d]{8}(-[a-f\d]{4}){4}[a-f\d]{8}$/i', $this->getId())) {
			throw new ValueException("Value \"".$this->getId()."\" is not a valid hook ID.");
		}
	}

	/**
	 * @return string[]
	 * @throws RequiredValueException
	 * @throws ValueException
	 */
	public function toArray(): array
	{
		$this->validate();

		return [
			'id' => $this->getId(),
		];
	}

	/**
	 * @return string
	 * @throws RequiredValueException
	 * @throws ValueException
	 */
	public function toJson(): string
	{
		if ($data = json_encode($this->toArray())) {
			return $data;
		}

		return '';
	}

	/**
	 * @return string|null
	 */
	public function getId(): ?string
	{
		return $this->id;
	}

	/**
	 * @param string $id
	 * @return $this
	 */
	public function setId(string $id): self
	{
		$this->id = $id;

		return $this;
	}
}
