<?php

namespace Connectix\Request;

use Connectix\Components\Message;
use Connectix\Exception;

class MessagesRequest extends BaseRequest
{
	/**
	 * @inheritDoc
	 */
	protected function getBasePath(): string
	{
		return 'messages';
	}

	/**
	 * @param Message $data
	 * @param array $query
	 * @return array|string
	 *
	 * @throws Exception\BadConnectionException
	 * @throws Exception\BadRequestException
	 * @throws Exception\BadResponseBodyException
	 * @throws Exception\BadTokenException
	 */
	public function send(Message $data, array $query = [])
	{
		return $this->call(null, $data, $query);
	}
}
