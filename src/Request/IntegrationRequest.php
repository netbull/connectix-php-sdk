<?php

namespace Connectix\Request;

use Connectix\Components\Integration;
use Connectix\Components\IntegrationUnregister;
use Connectix\Exception;

class IntegrationRequest extends BaseRequest
{
	/**
	 * @inheritDoc
	 */
	protected function getBasePath(): string
	{
		return 'integration';
	}

	/**
	 * @param Integration $data
	 * @param array $query
	 * @return array|string
	 *
	 * @throws Exception\BadConnectionException
	 * @throws Exception\BadRequestException
	 * @throws Exception\BadResponseBodyException
	 * @throws Exception\BadTokenException
	 */
	public function register(Integration $data, array $query = [])
	{
		return $this->call('/register', $data, $query);
	}

	/**
	 * @param IntegrationUnregister $data
	 * @param array $query
	 * @return array|string
	 *
	 * @throws Exception\BadConnectionException
	 * @throws Exception\BadRequestException
	 * @throws Exception\BadResponseBodyException
	 * @throws Exception\BadTokenException
	 */
	public function unregister(IntegrationUnregister $data, array $query = [])
	{
		return $this->call('/unregister', $data, $query);
	}
}
