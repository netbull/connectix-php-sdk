<?php

namespace Connectix\Request;

use Connectix\Exception;

class CountriesRequest extends BaseRequest
{
	/**
	 * @inheritDoc
	 */
	protected function getBasePath(): string
	{
		return 'countries';
	}

	/**
	 * @param string $country ISO2 country code
	 * @return string[] Array of dates in format YYYY-MM-DD
	 * @throws Exception\BadConnectionException
	 * @throws Exception\BadRequestException
	 * @throws Exception\BadResponseBodyException
	 * @throws Exception\BadTokenException
	 */
	public function fetchHolidays(string $country): array
	{
		return $this->call('/holidays', null, ['country' => $country]);
	}
}
