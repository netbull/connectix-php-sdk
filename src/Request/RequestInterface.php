<?php

namespace Connectix\Request;

use Connectix\Exception;
use Connectix\Components\ComponentInterface;

interface RequestInterface
{
	/**
	 * @return string
	 */
	public function getToken(): string;

	/**
	 * @param string $token
	 * @return RequestInterface
	 */
	public function setToken(string $token): self;

	/**
	 * @return bool
	 */
	public function isDev(): bool;

	/**
	 * @param bool $isDev
	 * @return RequestInterface
	 */
	public function setIsDev(bool $isDev): self;

	/**
	 * @param string|null $uri
	 * @param array|ComponentInterface|null $data
	 * @param array $query
	 * @return array|string
	 * @throws Exception\BadConnectionException
	 * @throws Exception\BadRequestException
	 * @throws Exception\BadResponseBodyException
	 * @throws Exception\BadTokenException
	 */
	public function call(string $uri = null, $data = null, array $query = []);
}
