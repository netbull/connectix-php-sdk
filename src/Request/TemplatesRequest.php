<?php

namespace Connectix\Request;

use Connectix\Exception;

class TemplatesRequest extends BaseRequest
{
	/**
	 * @inheritDoc
	 */
	protected function getBasePath(): string
	{
		return 'templates';
	}

	/**
	 * @param array $query
	 * @return array|string
	 *
	 * @throws Exception\BadConnectionException
	 * @throws Exception\BadRequestException
	 * @throws Exception\BadResponseBodyException
	 * @throws Exception\BadTokenException
	 */
	public function fetchTemplates(array $query = [])
	{
		return $this->call(null, null, $query);
	}
}
