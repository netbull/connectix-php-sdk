<?php

namespace Connectix\Request;

use Connectix\Components\IntegrationHook;
use Connectix\Components\IntegrationHookUnregister;
use Connectix\Exception;

class IntegrationHookRequest extends BaseRequest
{
	/**
	 * @inheritDoc
	 */
	protected function getBasePath(): string
	{
		return 'integration';
	}

	/**
	 * @param IntegrationHook $data
	 * @param array $query
	 * @return array|string
	 *
	 * @throws Exception\BadConnectionException
	 * @throws Exception\BadRequestException
	 * @throws Exception\BadResponseBodyException
	 * @throws Exception\BadTokenException
	 */
	public function register(IntegrationHook $data, array $query = [])
	{
		return $this->call('/register/hook', $data, $query);
	}

	/**
	 * @param IntegrationHookUnregister $data
	 * @param array $query
	 * @return array|string
	 *
	 * @throws Exception\BadConnectionException
	 * @throws Exception\BadRequestException
	 * @throws Exception\BadResponseBodyException
	 * @throws Exception\BadTokenException
	 */
	public function unregister(IntegrationHookUnregister $data, array $query = [])
	{
		return $this->call('/unregister/hook', $data, $query);
	}
}
