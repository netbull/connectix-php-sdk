<?php

namespace Connectix\Request;

use Connectix\Components\ComponentInterface;
use Connectix\Exception;

abstract class BaseRequest implements RequestInterface
{
	const LIVE_ENDPOINT = 'https://api.connectix.bg/';
	const SANDBOX_ENDPOINT = 'https://api-sandbox.connectix.bg/';

	/**
	 * @var string
	 */
	private string $token = '';

	/**
	 * @var bool
	 */
	private bool $isDev = true;

	/**
	 * @param string $token
	 * @param bool $isDev
	 */
	public function __construct(string $token, bool $isDev = true)
	{
		$this->token = $token;
		$this->isDev = $isDev;
	}

	/**
	 * @return string
	 */
	public function getToken(): string
	{
		return $this->token;
	}

	/**
	 * @param string $token
	 * @return BaseRequest
	 */
	public function setToken(string $token): self
	{
		$this->token = $token;

		return $this;
	}

	/**
	 * @return bool
	 */
	public function isDev(): bool
	{
		return $this->isDev;
	}

	/**
	 * @param bool $isDev
	 * @return BaseRequest
	 */
	public function setIsDev(bool $isDev): self
	{
		$this->isDev = $isDev;

		return $this;
	}

	/**
	 * @return string
	 */
	abstract protected function getBasePath(): string;

	/**
	 * @return string
	 */
	private function getEndpoint(): string
	{
		return $this->isDev ? self::SANDBOX_ENDPOINT : self::LIVE_ENDPOINT;
	}

	/**
	 * @param string|null $uri
	 * @param array|ComponentInterface|null $data
	 * @param array $query
	 * @return array|string
	 * @throws Exception\BadConnectionException
	 * @throws Exception\BadRequestException
	 * @throws Exception\BadResponseBodyException
	 * @throws Exception\BadTokenException
	 */
	public function call(string $uri = null, $data = null, array $query = [])
	{
		$headers = [
			"Content-Type: application/json",
			"Authorization: {$this->token}",
		];

		$url = $this->getEndpoint().$this->getBasePath();
		if ($uri) {
			$url .= $uri;
		}
		if (!empty($query)) {
			$url .= '?' . http_build_query($query);
		}

		$ch = curl_init();
		$options = [
			CURLOPT_URL => $url,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_FOLLOWLOCATION => false,
			CURLOPT_HTTPHEADER => $headers,
			CURLOPT_TIMEOUT => 15
		];

		if ($data instanceof ComponentInterface) {
			$options[CURLOPT_POST] = true;
			$options[CURLOPT_POSTFIELDS] = $data->toJson();
		} elseif (is_array($data)) {
			$options[CURLOPT_POST] = true;
			$options[CURLOPT_POSTFIELDS] = json_encode($data);
		}

		curl_setopt_array($ch, $options);
		$response = curl_exec($ch);

		if (!curl_errno($ch)) {
			$httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
			switch ($httpCode) {
				case 200:
					break;
				case 401:
					throw new Exception\BadTokenException();
				default:
					throw new Exception\BadRequestException($httpCode, $response);
			}
		} else {
			throw new Exception\BadConnectionException();
		}

		curl_close($ch);

		$content = json_decode($response, true);

		if (null === $content && json_last_error() !== JSON_ERROR_NONE) {
			throw new Exception\BadResponseBodyException();
		}

		return $content;
	}
}
