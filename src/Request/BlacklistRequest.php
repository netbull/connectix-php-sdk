<?php

namespace Connectix\Request;

use Connectix\Components\Blacklist;
use Connectix\Exception;

class BlacklistRequest extends BaseRequest
{
	/**
	 * @inheritDoc
	 */
	protected function getBasePath(): string
	{
		return 'blacklist';
	}

	/**
	 * @param Blacklist $data
	 * @param array $query
	 * @return string yes|no
	 * @throws Exception\BadConnectionException
	 * @throws Exception\BadRequestException
	 * @throws Exception\BadResponseBodyException
	 * @throws Exception\BadTokenException
	 */
	public function isBlacklisted(Blacklist $data, array $query = []): string
	{
		return $this->call(null, $data, $query);
	}
}
