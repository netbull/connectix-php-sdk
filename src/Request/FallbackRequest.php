<?php

namespace Connectix\Request;

use Connectix\Components\Fallback;
use Connectix\Exception;

class FallbackRequest extends BaseRequest
{
	/**
	 * @inheritDoc
	 */
	protected function getBasePath(): string
	{
		return 'messages/fallback';
	}

	/**
	 * @param Fallback $data
	 * @param array $query
	 * @return array|string
	 *
	 * @throws Exception\BadConnectionException
	 * @throws Exception\BadRequestException
	 * @throws Exception\BadResponseBodyException
	 * @throws Exception\BadTokenException
	 */
	public function send(Fallback $data, array $query = [])
	{
		return $this->call(null, $data, $query);
	}
}
