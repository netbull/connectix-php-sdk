require('dotenv').config();
const axios = require('axios');
const FormData = require('form-data');

const type = process.env.TYPE;
const language = process.env.LANGUAGE;
const version = process.env.VERSION;
const registertoken = process.env.REGISTER_TOKEN;

const init = async () => {
	try {
		const data = new FormData();
		data.append('fileName', '');
		data.append('version', version);

		await axios.post(`https://connectix.bg/integration/register/${type}-${language}`, data, {
			headers: {
				...data.getHeaders(),
				token: registertoken,
			}
		});
	} catch (e) {
		throw e;
	}
};

if (registertoken) {
	init().catch(() => {});
}
